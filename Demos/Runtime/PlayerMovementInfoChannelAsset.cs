/**
 * MuffinDev © 2022
 * Author: Hubert GROSSIN - dev@hubertg.fr
 */

#if MUFFINDEV_DEMOS

using MuffinDev.Broadcaster;

namespace MuffinDev.Demos.Broadcaster
{

    /// <summary>
    /// Represents an event channel that emits information about the movement of a player.
    /// </summary>
    [Menu("Demos/Player Movement Info")]
    public class PlayerMovementInfoChannelAsset : ChannelAssetBase<PlayerMovementInfo> { }

}

#endif