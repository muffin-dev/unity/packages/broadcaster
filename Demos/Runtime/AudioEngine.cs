/**
 * MuffinDev © 2022
 * Author: Hubert GROSSIN - dev@hubertg.fr
 */

#if MUFFINDEV_DEMOS

using UnityEngine;

using MuffinDev.Core;
using MuffinDev.Broadcaster;

namespace MuffinDev.Demos.Broadcaster
{

    /// <summary>
    /// This component illustrates how to listen for channel events.
    /// </summary>
    [AddComponentMenu(Constants.ADD_COMPONENT_MENU_DEMOS + "/Broadcaster/Demo - Audio Engine")]
    [HelpURL(Constants.BASE_HELP_URL)]
    public class AudioEngine : MonoBehaviour
    {

        [Tooltip("The channel asset to listen to receive an event when the player jumps.")]
        public ChannelAssetBase onPlayerJumpChannel;

        [Tooltip("The channel asset to listen to receive an event with useful data when the player moves.")]
        public PlayerMovementInfoChannelAsset onPlayerMoveChannel;

        /// <summary>
        /// Called when this component is enabled.
        /// </summary>
        private void OnEnable()
        {
            onPlayerJumpChannel.AddListener(PlayJumpSound);
            onPlayerMoveChannel.AddListener(PlayMoveSound);
        }

        /// <summary>
        /// Called when this component is disabled.
        /// </summary>
        private void OnDisable()
        {
            onPlayerJumpChannel.RemoveListener(PlayJumpSound);
            onPlayerMoveChannel.RemoveListener(PlayMoveSound);
        }

        public void PlayJumpSound()
        {
            Debug.Log("Playing jump sound");
        }

        public void PlayMoveSound(PlayerMovementInfo info)
        {
            Debug.Log($"Playing move sound for player {info.player.name} (speed = {info.speed})");
        }

    }

}

#endif