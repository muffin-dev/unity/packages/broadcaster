/**
 * MuffinDev © 2022
 * Author: Hubert GROSSIN - dev@hubertg.fr
 */

#if MUFFINDEV_DEMOS

using UnityEngine;

namespace MuffinDev.Demos.Broadcaster
{

    /// <summary>
    /// Groups informations about the movement of a <see cref="Player"/>.
    /// </summary>
    [System.Serializable]
    public struct PlayerMovementInfo
    {
        public Player player;
        public float speed;
        public Vector3 direction;
    }

}

#endif