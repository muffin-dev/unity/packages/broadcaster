/**
 * MuffinDev © 2022
 * Author: Hubert GROSSIN - dev@hubertg.fr
 */

#if MUFFINDEV_DEMOS

using UnityEngine;

using MuffinDev.Core;
using MuffinDev.Broadcaster;

namespace MuffinDev.Demos.Broadcaster
{

    /// <summary>
    /// This component illustrates how to invoke channel assets.
    /// </summary>
    [AddComponentMenu(Constants.ADD_COMPONENT_MENU_DEMOS + "/Broadcaster/Demo - Player")]
    [HelpURL(Constants.BASE_HELP_URL)]
    public class Player : MonoBehaviour
    {

        [Tooltip("The channel asset to invoke when this player jumps. Note that ChannelAssetBase is the highest class in the hierarchy, so this event could be any type of channel.")]
        public ChannelAssetBase onJumpChannel;

        [Tooltip("The channel asset to invoke when this player moves.")]
        public PlayerMovementInfoChannelAsset onMoveChannel;

        [Tooltip("Defines this player movement speed.")]
        public float speed = 6f;

        /// <summary>
        /// Called the first time this component is enabled.
        /// </summary>
        private void Start()
        {
            // Will Jump() 2 seconds after the game starts
            Invoke(nameof(Jump), 2f);
            // Will Move() 1 second after the game starts
            Invoke(nameof(Move), 1f);
        }

        public void Jump()
        {
            Debug.Log("Player jumps!");
            onJumpChannel.Invoke();
        }

        public void Move()
        {
            Debug.Log("Player moves!");
            onMoveChannel.Invoke(new PlayerMovementInfo
            {
                player = this,
                direction = Vector3.right,
                speed = speed
            });

            // Will call Move() again 1 second later
            Invoke(nameof(Move), 1f);
        }

    }

}

#endif