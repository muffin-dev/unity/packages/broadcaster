/**
 * MuffinDev © 2022
 * Author: Hubert GROSSIN - dev@hubertg.fr
 */

#if MUFFINDEV_DEMOS

using UnityEditor;

using MuffinDev.Broadcaster.EditorOnly;

namespace MuffinDev.Demos.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="PlayerMovementInfoChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(PlayerMovementInfoChannelAsset))]
    public class PlayerMovementInfoChannelAssetEditor : ChannelAssetT0EditorBase<PlayerMovementInfo> { }

}

#endif