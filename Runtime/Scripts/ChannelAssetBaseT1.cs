/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents a channel for an event with two parameters that can be invoked and trigger callbacks.
    /// </summary>
    public abstract class ChannelAssetBase<T0, T1> : ChannelAssetBaseGeneric
    {

        [SerializeField]
        [Tooltip("Called when this channel is triggered.")]
        private UnityEvent<T0, T1> _onTrigger = new UnityEvent<T0, T1>();

        /// <inheritdoc cref="ChannelAssetBase.Invoke"/>
        public void Invoke(T0 param0, T1 param1)
        {
            OnTriggerParameterless.Invoke();
            _onTrigger.Invoke(param0, param1);
        }

        /// <summary>
        /// Invokes this channel event using default parameter values.
        /// </summary>
        public override void Invoke()
        {
            base.Invoke();
            _onTrigger.Invoke(default, default);
        }

        /// <inheritdoc cref="ChannelAssetBase.AddListener(UnityAction)"/>
        public void AddListener(UnityAction<T0, T1> call)
        {
            _onTrigger.AddListener(call);
        }

        /// <inheritdoc cref="ChannelAssetBase.AddListener(UnityAction)"/>
        public override void AddListener(UnityAction call)
        {
            base.AddListener(call);
        }

        /// <inheritdoc cref="ChannelAssetBase.RemoveListener(UnityAction)"/>
        public void RemoveListener(UnityAction<T0, T1> call)
        {
            _onTrigger.RemoveListener(call);
        }

        /// <inheritdoc cref="ChannelAssetBase.RemoveListener(UnityAction)"/>
        public override void RemoveListener(UnityAction call)
        {
            base.RemoveListener(call);
        }

        /// <inheritdoc cref="UnityEventBase.RemoveAllListeners"/>
        public override void RemoveAllListeners()
        {
            base.RemoveAllListeners();
            _onTrigger.RemoveAllListeners();
        }

    }

}