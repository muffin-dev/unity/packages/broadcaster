/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a <see cref="Object"/> parameter.
    /// </summary>
    [Menu("Object")]
    public class ObjectChannelAsset : ChannelAssetBase<Object> { }

}