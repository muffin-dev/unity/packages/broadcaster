/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a float parameter.
    /// </summary>
    [Menu("Float")]
    public class FloatChannelAsset : ChannelAssetBase<float> { }

}