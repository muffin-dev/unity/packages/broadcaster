/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a <see cref="Vector3"/> parameter.
    /// </summary>
    [Menu("Vector3")]
    public class Vector3ChannelAsset : ChannelAssetBase<Vector3> { }

}