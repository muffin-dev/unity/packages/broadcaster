/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a <see cref="Vector2"/> parameter.
    /// </summary>
    [Menu("Vector2")]
    public class Vector2ChannelAsset : ChannelAssetBase<Vector2> { }

}