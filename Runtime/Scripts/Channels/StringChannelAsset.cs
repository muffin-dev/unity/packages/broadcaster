/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a string parameter.
    /// </summary>
    [Menu("String")]
    public class StringChannelAsset : ChannelAssetBase<string> { }

}