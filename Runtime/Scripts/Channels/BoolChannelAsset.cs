/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a boolean parameter.
    /// </summary>
    [Menu("Boolean")]
    public class BoolChannelAsset : ChannelAssetBase<bool> { }

}