/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with no parameters.
    /// </summary>
    [Menu("Void")]
    public class VoidChannelAsset : ChannelAssetBase
    {

        [SerializeField]
        [Tooltip("Called when this channel is triggered.")]
        private UnityEvent _onTrigger = new UnityEvent();

        /// <inheritdoc cref="ChannelAssetBase.Invoke"/>
        public override void Invoke()
        {
            _onTrigger.Invoke();
        }

        /// <inheritdoc cref="ChannelAssetBase.AddListener(UnityAction)"/>
        public override void AddListener(UnityAction call)
        {
            _onTrigger.AddListener(call);
        }

        /// <inheritdoc cref="ChannelAssetBase.RemoveListener(UnityAction)"/>
        public override void RemoveListener(UnityAction call)
        {
            _onTrigger.RemoveListener(call);
        }

        /// <inheritdoc cref="ChannelAssetBase.RemoveAllListeners"/>
        public override void RemoveAllListeners()
        {
            _onTrigger.RemoveAllListeners();
        }

    }

}