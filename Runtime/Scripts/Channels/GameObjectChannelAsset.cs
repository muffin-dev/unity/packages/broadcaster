/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with a <see cref="GameObject"/> parameter.
    /// </summary>
    [Menu("GameObject")]
    public class GameObjectChannelAsset : ChannelAssetBase<GameObject> { }

}