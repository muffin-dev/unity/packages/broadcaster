/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Represents an event channel with an integer parameter.
    /// </summary>
    [Menu("Int")]
    public class IntChannelAsset : ChannelAssetBase<int> { }

}