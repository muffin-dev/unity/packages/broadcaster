/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

using MuffinDev.Core;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Container for event channel assets.
    /// </summary>
    [CreateAssetMenu(fileName = "NewBroadcaster", menuName = Constants.CREATE_ASSET_MENU + "/Broadcaster")]
    public class BroadcasterAsset : ScriptableObject
    {

        #region Fields

        [SerializeField]
        [Tooltip("The list of all the channels on this broadcaster.")]
        private ChannelAssetBase[] _channels = { };

        [SerializeField, TextArea(4, 10)]
        [Tooltip("Description of this broadcaster.")]
        private string _description = string.Empty;

        #endregion


        #region Properties

        /// <inheritdoc cref="_channels"/>
        public ChannelAssetBase[] Channels => _channels;

        /// <inheritdoc cref="_description"/>
        public string Description => _description;

        #endregion

    }

}