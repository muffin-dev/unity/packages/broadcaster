/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine.Events;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Base class to create channel assets that represent an event with parameters that can be invoked and trigger callbacks.
    /// </summary>
    public abstract class ChannelAssetBaseGeneric : ChannelAssetBase
    {

        /// <summary>
        /// Hidden event that handle parameterless calls when this channel is invoked.
        /// </summary>
        private UnityEvent _onTriggerParameterless = new UnityEvent();

        /// <summary>
        /// Invokes this channel event using default parameter values.
        /// </summary>
        public override void Invoke()
        {
            _onTriggerParameterless.Invoke();
        }

        /// <inheritdoc cref="ChannelAssetBase.AddListener(UnityAction)"/>
        public override void AddListener(UnityAction call)
        {
            _onTriggerParameterless.AddListener(call);
        }

        /// <inheritdoc cref="UnityEvent{T0}.RemoveListener(UnityAction{T0})"/>
        public override void RemoveListener(UnityAction call)
        {
            _onTriggerParameterless.RemoveListener(call);
        }

        /// <inheritdoc cref="UnityEventBase.RemoveAllListeners"/>
        public override void RemoveAllListeners()
        {
            _onTriggerParameterless.RemoveAllListeners();
        }

        /// <inheritdoc cref="_onTriggerParameterless"/>
        protected UnityEvent OnTriggerParameterless => _onTriggerParameterless;

    }

}