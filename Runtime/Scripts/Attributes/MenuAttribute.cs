/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Defines the path to create a channel of this type when adding one from the editor of a <see cref="BroadcasterAsset"/>. You can
    /// create sub-menus by separating menus with "/".
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class MenuAttribute : Attribute
    {

        /// <summary>
        /// The menu to create a channel asset of that type.
        /// </summary>
        private string _path = string.Empty;

        /// <summary>
        /// Defines the path to create a channel of this type when adding one from the editor of a <see cref="BroadcasterAsset"/>.
        /// </summary>
        /// <param name="path">The menu to create a channel of that type. You can create sub-menus by separating parts with "/".</param>
        public MenuAttribute(string path)
        {
            _path = path;
        }

        /// <inheritdoc cref="_path"/>
        public string Path => _path;

    }

}