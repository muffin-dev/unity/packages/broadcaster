/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Broadcaster
{

    /// <summary>
    /// Base class that represents a channel that can be invoked and trigger callbacks.
    /// </summary>
    public abstract class ChannelAssetBase : ScriptableObject
    {

        #region Fields

        [SerializeField, TextArea(4, 10)]
        [Tooltip("Description of this channel.")]
        private string _description = string.Empty;

        #endregion


        #region Public API

        /// <inheritdoc cref="_description"/>
        public string Description => _description;

        /// <summary>
        /// Invokes this channel event.
        /// </summary>
        public abstract void Invoke();

        /// <inheritdoc cref="UnityEvent.AddListener(UnityAction)"/>
        public abstract void AddListener(UnityAction call);

        /// <inheritdoc cref="UnityEvent.RemoveListener(UnityAction)"/>
        public abstract void RemoveListener(UnityAction call);

        /// <inheritdoc cref="UnityEventBase.RemoveAllListeners()"/>
        public abstract void RemoveAllListeners();

        #endregion

    }

}