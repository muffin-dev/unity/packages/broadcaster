/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Miscellanous utility function for drawing fields for <see cref="ChannelAsset"/> custom editors.
    /// </summary>
    public static class ChannelAssetEditorGUILayout
    {

        #region Delegates

        /// <summary>
        /// Called to draw the custom fields of a manual trigger area (drawn using
        /// <see cref="ManualTriggerArea(DrawManualTriggerAreaInvokeDelegate)"/>.
        /// </summary>
        public delegate void DrawManualTriggerAreaContentDelegate();

        /// <summary>
        /// Called when the user clicks on the "trigger" button of a manual trigger area (drawn using
        /// <see cref="ManualTriggerArea(DrawManualTriggerAreaInvokeDelegate)"/>.
        /// </summary>
        public delegate void DrawManualTriggerAreaInvokeDelegate();

        #endregion


        #region Public API

        /// <summary>
        /// Draws an area with a button to manually trigger a channel asset.
        /// </summary>
        /// <param name="onDrawContent">Called to draw the custom fields for this area.</param>
        /// <param name="onInvoke">Called when the user clicks on the "trigger" button.</param>
        public static void ManualTriggerArea(DrawManualTriggerAreaContentDelegate onDrawContent, DrawManualTriggerAreaInvokeDelegate onInvoke)
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                EditorGUILayout.LabelField("Manual Trigger", EditorStyles.boldLabel);

                if (onDrawContent != null)
                {
                    using (new IndentedScope())
                    {
                        onDrawContent.Invoke();
                    }
                }

                EditorGUILayout.Space();
                if (GUILayout.Button(new GUIContent("Trigger", "Calls the Invoke() method of this channel.")))
                {
                    onInvoke?.Invoke();
                }
            }
        }

        /// <inheritdoc cref="ManualTriggerArea(DrawManualTriggerAreaContentDelegate, DrawManualTriggerAreaInvokeDelegate)"/>
        public static void ManualTriggerArea(DrawManualTriggerAreaInvokeDelegate onInvoke)
        {
            ManualTriggerArea(null, onInvoke);
        }

        #endregion

    }

}