/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Base custom editor for <see cref="ChannelAssetBase{T0, T1, T2, T3}"/> objects.
    /// </summary>
    public abstract class ChannelAssetT1EditorBase<T0, T1, T2, T3> : TEditor<ChannelAssetBase<T0, T1, T2, T3>>
    {

        #region Fields

        [SerializeField]
        [Tooltip("The value you want to use as first parameter for manual triggering.")]
        private T0 _param0 = default;

        [SerializeField]
        [Tooltip("The value you want to use as second parameter for manual triggering.")]
        private T1 _param1 = default;

        [SerializeField]
        [Tooltip("The value you want to use as thrid parameter for manual triggering.")]
        private T2 _param2 = default;

        [SerializeField]
        [Tooltip("The value you want to use as thrid parameter for manual triggering.")]
        private T3 _param3 = default;

        private SerializedObject _serializedEditor = null;
        private SerializedProperty _triggerParam0Prop = null;
        private SerializedProperty _triggerParam1Prop = null;
        private SerializedProperty _triggerParam2Prop = null;
        private SerializedProperty _triggerParam3Prop = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is loaded.
        /// </summary>
        private void OnEnable()
        {
            hideFlags = HideFlags.None;
            _serializedEditor = new SerializedObject(this);
            _triggerParam0Prop = _serializedEditor.FindProperty(nameof(_param0));
            _triggerParam1Prop = _serializedEditor.FindProperty(nameof(_param1));
            _triggerParam2Prop = _serializedEditor.FindProperty(nameof(_param2));
            _triggerParam3Prop = _serializedEditor.FindProperty(nameof(_param3));
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this custom inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            ChannelAssetEditorGUILayout.ManualTriggerArea
            (
                () =>
                {
                    EditorGUILayout.PropertyField(_triggerParam0Prop);
                    EditorGUILayout.PropertyField(_triggerParam1Prop);
                    EditorGUILayout.PropertyField(_triggerParam2Prop);
                    EditorGUILayout.PropertyField(_triggerParam3Prop);
                },
                () => target.Invoke(_param0, _param1, _param2, _param3)
            );
        }

        #endregion

    }

}