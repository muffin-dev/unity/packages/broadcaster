/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Base custom editor for <see cref="ChannelAssetBase{T0}"/> objects.
    /// </summary>
    public abstract class ChannelAssetT0EditorBase<T0> : TEditor<ChannelAssetBase<T0>>
    {

        #region Fields

        [SerializeField]
        [Tooltip("The value you want to use for manual triggering.")]
        private T0 _param = default;

        private SerializedObject _serializedEditor = null;
        private SerializedProperty _triggerParamProp = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is loaded.
        /// </summary>
        private void OnEnable()
        {
            hideFlags = HideFlags.None;
            _serializedEditor = new SerializedObject(this);
            _triggerParamProp = _serializedEditor.FindProperty(nameof(_param));
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this custom inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            ChannelAssetEditorGUILayout.ManualTriggerArea
            (
                () => EditorGUILayout.PropertyField(_triggerParamProp),
                () => target.Invoke(_param)
            );
        }

        #endregion

    }

}