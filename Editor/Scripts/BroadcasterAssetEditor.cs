/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using MuffinDev.Core;
using MuffinDev.Core.EditorOnly;
using MuffinDev.Core.Reflection;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="BroadcasterAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(BroadcasterAsset))]
    public class BroadcasterAssetEditor : Editor
    {

        #region Fields

        private const string CHANNELS_PROP = "_channels";
        private const string DESCRIPTION_PROP = "_description";

        /// <summary>
        /// The list of menus associated with the channel asset type to create.
        /// </summary>
        private static SortedDictionary<string, Type> s_channelAssetMenus = null;

        /// <summary>
        /// Channel assets list as reorderable list.
        /// </summary>
        private ReorderableList _channelsReorderableList = null;

        private SerializedProperty _channelsProp = null;
        private SerializedProperty _descriptionProp = null;

        private Editor _selectedChannelAssetEditor = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is displayed.
        /// </summary>
        private void OnEnable()
        {
            _channelsProp = serializedObject.FindProperty(CHANNELS_PROP);
            _descriptionProp = serializedObject.FindProperty(DESCRIPTION_PROP);
        }

        /// <summary>
        /// Called when this editor is closed.
        /// </summary>
        private void OnDisable()
        {
            if (_selectedChannelAssetEditor != null)
                DestroyImmediate(_selectedChannelAssetEditor);
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this editor GUI in the inspector.
        /// </summary>
        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField(_descriptionProp);
            EditorGUILayout.Space();
            ChannelsReorderableList.DoLayoutList();

            if (_selectedChannelAssetEditor != null)
            {
                EditorGUILayout.Space();
                MoreEditorGUI.HorizontalSeparator(true);
                EditorGUILayout.Space();
                _selectedChannelAssetEditor.OnInspectorGUI();
            }

            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Draws the header of the channel assets list.
        /// </summary>
        private void DrawChannelsListHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, new GUIContent(_channelsProp.displayName, _channelsProp.tooltip));
        }

        /// <summary>
        /// Draws a channel asset item field in the list.
        /// </summary>
        private void DrawChannelItem(Rect rect, int index, bool isActive, bool focused)
        {
            rect.y += (rect.height - EditorGUIUtility.singleLineHeight) / 2;
            rect.height = EditorGUIUtility.singleLineHeight;

            // Draw index label
            Rect tmpRect = new Rect(rect);
            tmpRect.width = MoreGUI.WIDTH_XS;
            EditorGUI.LabelField(tmpRect, index.ToString());

            // Draw channel name field
            tmpRect.x += tmpRect.width + MoreGUI.H_MARGIN;
            tmpRect.width = rect.width - tmpRect.width - MoreGUI.H_MARGIN;
            SerializedProperty channelAssetProp = _channelsProp.GetArrayElementAtIndex(index);
            if (channelAssetProp.objectReferenceValue != null)
            {
                string newName = EditorGUI.DelayedTextField(tmpRect, channelAssetProp.objectReferenceValue.name);
                if (newName != channelAssetProp.objectReferenceValue.name)
                {
                    channelAssetProp.objectReferenceValue.name = newName;
                    SerializedObject obj = new SerializedObject(channelAssetProp.objectReferenceValue);
                    obj.ApplyModifiedProperties();
                    AssetDatabase.SaveAssets();
                    AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(target), ImportAssetOptions.ForceUpdate);
                }
            }
            else
            {
                EditorGUI.HelpBox(tmpRect, "Missing channel asset", MessageType.Warning);
            }
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="s_channelAssetMenus"/>
        private static SortedDictionary<string, Type> ChannelAssetMenus
        {
            get
            {
                if (s_channelAssetMenus == null)
                {
                    s_channelAssetMenus = new SortedDictionary<string, Type>();
                    foreach (Type t in ReflectionUtility.GetAllTypesInProjectAssignableFrom<ChannelAssetBase>())
                    {
                        if (t.IsAbstract)
                            continue;

                        string menu = ObjectNames.NicifyVariableName(t.Name);
                        if (t.TryGetAttribute(out MenuAttribute menuAttribute) && !string.IsNullOrEmpty(menuAttribute.Path))
                        {
                            menu = menuAttribute.Path;
                        }
                        s_channelAssetMenus.Add(menu, t);
                    }
                }
                return s_channelAssetMenus;
            }
        }

        /// <summary>
        /// Gets the channel assets list as reorderable list.
        /// </summary>
        private ReorderableList ChannelsReorderableList
        {
            get
            {
                if (_channelsReorderableList == null)
                {
                    _channelsReorderableList = new ReorderableList(serializedObject, _channelsProp);
                    _channelsReorderableList.drawHeaderCallback = DrawChannelsListHeader;
                    _channelsReorderableList.onAddDropdownCallback = DisplayAddChannelMenu;
                    _channelsReorderableList.onRemoveCallback = RemoveChannel;
                    _channelsReorderableList.drawElementCallback = DrawChannelItem;
                    _channelsReorderableList.onSelectCallback += SelectChannelItem;
                }
                return _channelsReorderableList;
            }
        }

        /// <summary>
        /// Adds the given channel asset to the list.
        /// </summary>
        /// <param name="channelAsset">The channel asset you want to add in the list.</param>
        /// <returns>Returns true if the given channel asset has been added to the list successfully, otherwise false.</returns>
        private bool AddChannelAsset(ChannelAssetBase channelAsset)
        {
            if (channelAsset == null)
            {
                return false;
            }

            // Attach the new channel asset to the edited broadcaster asset
            AssetDatabase.AddObjectToAsset(channelAsset, target);
            // Add it to the list
            int index = _channelsProp.arraySize;
            _channelsProp.InsertArrayElementAtIndex(index);
            _channelsProp.GetArrayElementAtIndex(index).objectReferenceValue = channelAsset;
            serializedObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(target));

            return true;
        }

        /// <summary>
        /// Display a context menu for adding a new channel asset to the list.
        /// </summary>
        private void DisplayAddChannelMenu(Rect buttonRect, ReorderableList list)
        {
            GenericMenu menu = new GenericMenu();
            foreach (KeyValuePair<string, Type> m in ChannelAssetMenus)
            {
                menu.AddItem(new GUIContent(m.Key), false, () =>
                {
                    ChannelAssetBase channelAsset = CreateInstance(m.Value) as ChannelAssetBase;
                    channelAsset.name = $"New{m.Value.Name}";
                    if (AddChannelAsset(channelAsset))
                    {
                        list.index = _channelsProp.arraySize - 1;
                        SelectChannelItem(list);
                    }
                });
            }

            menu.ShowAsContext();
        }

        /// <summary>
        /// Removes the selected channel asset from the list.
        /// </summary>
        private void RemoveChannel(ReorderableList list)
        {
            if (_selectedChannelAssetEditor != null)
                DestroyImmediate(_selectedChannelAssetEditor);

            SerializedProperty channelAssetProp = _channelsProp.GetArrayElementAtIndex(list.index);
            if (channelAssetProp.objectReferenceValue != null)
            {
                DestroyImmediate(channelAssetProp.objectReferenceValue, true);
                AssetDatabase.SaveAssets();
            }

            _channelsProp.DeleteArrayElementAtIndex(list.index);

            // Force select the item that was previously at this index
            SelectChannelItem(list);
        }

        /// <summary>
        /// Called when the selected channel asset in the list changes.
        /// </summary>
        private void SelectChannelItem(ReorderableList list)
        {
            if (_selectedChannelAssetEditor != null)
                DestroyImmediate(_selectedChannelAssetEditor);

            if (list.index >= 0 && _channelsProp.arraySize > 0 && list.index < _channelsProp.arraySize)
            {
                SerializedProperty prop = _channelsProp.GetArrayElementAtIndex(list.index);
                if (prop.objectReferenceValue != null)
                    _selectedChannelAssetEditor = CreateEditor(prop.objectReferenceValue);
            }
        }

        #endregion

    }

}