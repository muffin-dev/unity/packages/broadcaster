/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Base custom editor for <see cref="ChannelAssetBase{T0, T1}"/> objects.
    /// </summary>
    public abstract class ChannelAssetT1EditorBase<T0, T1> : TEditor<ChannelAssetBase<T0, T1>>
    {

        #region Fields

        [SerializeField]
        [Tooltip("The value you want to use as first parameter for manual triggering.")]
        private T0 _param0 = default;

        [SerializeField]
        [Tooltip("The value you want to use as second parameter for manual triggering.")]
        private T1 _param1 = default;

        private SerializedObject _serializedEditor = null;
        private SerializedProperty _triggerParam0Prop = null;
        private SerializedProperty _triggerParam1Prop = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is loaded.
        /// </summary>
        private void OnEnable()
        {
            hideFlags = HideFlags.None;
            _serializedEditor = new SerializedObject(this);
            _triggerParam0Prop = _serializedEditor.FindProperty(nameof(_param0));
            _triggerParam1Prop = _serializedEditor.FindProperty(nameof(_param1));
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this custom inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            ChannelAssetEditorGUILayout.ManualTriggerArea
            (
                () =>
                {
                    EditorGUILayout.PropertyField(_triggerParam0Prop);
                    EditorGUILayout.PropertyField(_triggerParam1Prop);
                },
                () => target.Invoke(_param0, _param1)
            );
        }

        #endregion

    }

}