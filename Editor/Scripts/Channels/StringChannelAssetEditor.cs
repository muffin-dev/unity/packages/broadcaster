/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="StringChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(StringChannelAsset))]
    public class StringChannelAssetEditor : ChannelAssetT0EditorBase<string> { }

}