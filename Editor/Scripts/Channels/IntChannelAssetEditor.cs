/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="IntChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(IntChannelAsset))]
    public class IntChannelAssetEditor : ChannelAssetT0EditorBase<int> { }

}