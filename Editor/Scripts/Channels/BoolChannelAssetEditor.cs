/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="BoolChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(BoolChannelAsset))]
    public class BoolChannelAssetEditor : ChannelAssetT0EditorBase<bool> { }

}