/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="GameObjectChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(GameObjectChannelAsset))]
    public class GameObjectChannelAssetEditor : ChannelAssetT0EditorBase<GameObject> { }

}