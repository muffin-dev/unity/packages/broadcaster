/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="Vector2ChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(Vector2ChannelAsset))]
    public class Vector2ChannelAssetEditor : ChannelAssetT0EditorBase<Vector2> { }

}