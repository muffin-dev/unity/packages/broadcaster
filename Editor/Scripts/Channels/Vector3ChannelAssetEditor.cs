/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="Vector3ChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(Vector3ChannelAsset))]
    public class Vector3ChannelAssetEditor : ChannelAssetT0EditorBase<Vector3> { }

}