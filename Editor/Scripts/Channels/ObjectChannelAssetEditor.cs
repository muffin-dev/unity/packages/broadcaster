/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="ObjectChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(ObjectChannelAsset))]
    public class ObjectChannelAssetEditor : ChannelAssetT0EditorBase<Object> { }

}