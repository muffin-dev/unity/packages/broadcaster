/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="FloatChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(FloatChannelAsset))]
    public class FloatChannelAssetEditor : ChannelAssetT0EditorBase<float> { }

}