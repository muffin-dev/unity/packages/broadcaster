/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;

using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="VoidChannelAsset"/> objects.
    /// </summary>
    [CustomEditor(typeof(VoidChannelAsset))]
    public class VoidChannelAssetEditor : TEditor<VoidChannelAsset>
    {

        #region UI

        /// <summary>
        /// Draws this custom inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            ChannelAssetEditorGUILayout.ManualTriggerArea(() => target.Invoke());
        }

        #endregion

    }

}