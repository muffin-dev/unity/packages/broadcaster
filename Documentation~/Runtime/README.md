# Broadcaster - Documentation - Runtime

## Summary

- [`BroadcasterAsset`](./broadcaster-asset.md): Container for event channel assets.
- [`ChannelAssetBase`](./channel-asset-base.md): Base class that represents a channel that can be invoked and trigger callbacks.
- [`MenuAttribute`](./menu-attribute.md): Defines the path to create a channel from the dropdown menu of a [`BroadcasterAsset`](./broadcaster-asset.md).

---

[<= Back to package's summary](../README.md)