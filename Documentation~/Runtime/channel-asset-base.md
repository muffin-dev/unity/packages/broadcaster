# Broadcaster - Runtime - `ChannelAssetBase`

Base class that represents a channel that can be invoked and trigger callbacks.

A channel asset can't be created directly. Any implementation of this class will be available in the dropdown menu when you want to add a new channel asset to a [`BroadcasterAsset`](./broadcaster-asset.md).

![Dropdown menu from Broadcaster asset to create channels](../Images/create-channel.png)

## Create custom channel types

This package provides several channel asset types for basic data (boolean, integer, ...). But there's some generic implementations ready to use if you want to create custom channels.

As an example, let's create a custom channel asset type for events that take a `Transform` component as parameter:

```cs
using UnityEngine;
using MuffinDev.Broadcaster;

[Menu("Custom/Transform")]
public class TransformChannelAsset : ChannelAssetBase<Transform> { }
```

![Custom menu preview](../Images/menu-attribute.png)

Note the use of [`MenuAttribute`](./menu-attribute.md) that allow you to customize the menu used to create the channel.

You can create channels for events with up to 4 parameters using generic implementations.

Note that this custom channel asset type doesn't have an appropriate inspector, so you won't be able to invoke the event with testing values from the editor. You can do so by implementing [`ChannelAssetTXEditorBase`](../Editor/channel-asset-editor-base.md), where X is the number of parameters of the event, starting at 0.

## Public API

```cs
public abstract class ChannelAssetBase : ScriptableObject { }
public abstract class ChannelAssetBase<T0> : ChannelAssetBaseGeneric { }
public abstract class ChannelAssetBase<T1> : ChannelAssetBaseGeneric { }
public abstract class ChannelAssetBase<T2> : ChannelAssetBaseGeneric { }
public abstract class ChannelAssetBase<T3> : ChannelAssetBaseGeneric { }
```

### Functions

#### `Invoke()`

```cs
public abstract void Invoke()
```

Invokes this channel event.

#### `AddListener()`

```cs
public abstract void AddListener(UnityAction call)
```

Add a non-persistent listener for the event.

#### `RemoveListener()`

```cs
public abstract void RemoveListener(UnityAction call)
```

Remove a non-persistent listener from the event. if you have added the same listener multiiple times, this method will remove all occurrences of it.

#### `RemoveAllListeners()`

```cs
public abstract void RemoveAllListeners()
```

Remove all non-persistent listeners from the event.

### Properties

#### `Description`

```cs
public string Description { get; }
```

Description of this channel.

---

[<= Back to summary](./README.md)