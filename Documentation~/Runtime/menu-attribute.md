# Broadcaster - Runtime - `MenuAttribute`

Defines the path to create a channel from the dropdown menu of a [`BroadcasterAsset`](./broadcaster-asset.md). You can create sub-menus by separating menus with `/`.

## Usage

```cs
using UnityEngine;
using MuffinDev.Broadcaster;

[Menu("Custom/Transform")]
public class TransformChannelAsset : ChannelAssetBase<Transform> { }
```

![Custom menu preview](../Images/menu-attribute.png)

## Public API

```cs
[AttributeUsage(AttributeTargets.Class)]
public class MenuAttribute : Attribute { }
```

### Constructors

```cs
public MenuAttribute(string path)
```

- `string path`: The menu to create a channel of that type. You can create sub-menus by separating parts with `/`.

### Properties

#### `Path`

```cs
public string Path { get; }
```

The menu to create a channel asset of that type.

---

[<= Back to summary](./README.md)