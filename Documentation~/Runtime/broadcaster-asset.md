# Broadcaster - Runtime - `BroadcasterAsset`

Container for event channel assets.

![Broadcaster asset inspector](../Images/broadcaster-asset.png)

You can create a `BroadcasterAsset` from `Assets > Create > Muffin Dev > Broadcaster`.

[=> Learn more about how to use Broadcaset assets in the editor](../getting-started.md)

## Public API

```cs
[CreateAssetMenu(fileName = "NewBroadcaster", menuName = Constants.CREATE_ASSET_MENU + "/Broadcaster")]
public class BroadcasterAsset : ScriptableObject { }
```

### Properties

#### `Channels`

```cs
public ChannelAssetBase[] Channels { get; }
```

The list of all the channels on this broadcaster.

#### `Description`

```cs
public string Description { get; }
```

Description of this broadcaster.

---

[<= Back to summary](./README.md)