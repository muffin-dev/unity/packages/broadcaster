# Muffin Dev for Unity - Broadcaster - Getting Started

## Philosophy

There's two main assets for *Broadcaster*:

- **Broadcaster Asset**: This is basically a "collection" of channels
- **Channel Asset**: Represents a single event that can be invoked and listened

Channels can represent events with or without parameters, but can always be invoked parameterless (using the default parameters values).

## Creating the assets

![Broadcaster Asset inspector](./Images/broadcaster-asset.png)

Create a [`BroadcasterAsset`](./Runtime/broadcaster-asset.md) from `Assets > Create > Muffin Dev > Broadcaster`, and name it `PlayerEvents`.

In the **Channels** list, click on the *+* icon to add a `ChannelAsset`. The dropdown lists all implementations of [`ChannelAssetBase`](./Runtime/channel-asset-base.md), which wraps an event. Select `Void` to create a channel for a parameterless events.

![Channel assets dropdown](./Images/create-channel.png)

The created `ChannelAsset` is attached to its `BroadcasterAsset`, and you can edit it from both the broadcaster asset inspector, or the channel asset itself in the project view. Also, in the `BroadcasterAsset` inspector, you can rename an event directly in the channels list. Rename your brand new channel asset `OnJump`.

![Channel Asset inspector from Broadcaster](./Images/channel-asset-example.png)
![Channel Asset in Project View](./Images/channel-asset-project-view.png)

## Invoking channel events

Create a new `MonoBehaviour` named `Player`, and copy the following script:

```cs
using UnityEngine;
using MuffinDev.Broadcaster;
public class Player : MonoBehaviour
{
    public ChannelAssetBase onJumpChannel;

    private void Start()
    {
        // Call Jump() 2 seconds after the game starts
        Invoke(nameof(Jump), 2f);
    }

    public void Jump()
    {
        Debug.Log("Player jumps!");
        onJumpChannel.Invoke();
    }
}
```

This script has a `ChannelAssetBase` property to define which channel asset to **invoke** when it jumps. The `Jump()` function, called 2 seconds after the game starts, will invoke the channel asset's event.

Add this component to a GameObject in the scene, and assign the `OnJump` channel asset to the **On Jump Channel** property in the inspector.

![The Player component with channel asset assigned](./Images/player-example.png)

If you run the game by now, you'll see the message *Player jumps!* in the Console, but nothing else will happen. This is because the event doesn't have any listener.

## Listening channel events

Create a new `MonoBehaviour` named `AudioEngine`, and copy the following script:

```cs
using UnityEngine;
using MuffinDev.Broadcaster;
public class AudioEngine : MonoBehaviour
{
    public ChannelAssetBase onJumpChannel;

    private void OnEnable()
    {
        onJumpChannel.AddListener(PlayJumpSound);
    }

    private void OnDisable()
    {
        onJumpChannel.RemoveListener(PlayJumpSound);
    }

    public void PlayJumpSound()
    {
        Debug.Log("Playing jump sound");
    }
}
```

This script has a `ChannelAssetBase` property, just like the `Player` component explained above, but this time it's meant to define which channel asset to **listen** when the player jumps.

In the `OnEnable()` function, the `PlayJumpSound()` is added as the listener for the channel event. Just like regular events, it means that this function wiill be called when the channel is invoked.

Notice the call to `RemoveListener()` in the `OnDisable` function. It's a good practice to add ***and*** remove listeners manually using these lifecycle callbacks, which ensures that the listener is not called if the component is disabled or destroyed.

Add this component to a GameObject in the scene, and assign the `OnJump` channel asset to the **On Jump Channel** property in the inspector.

![The components with channel asset assigned](./Images/audio-engine-example.png)

If you run the game and wait 2 seconds, you should see two messages in the Console: *Player jumps!* and *Playing jump sound*. The first messages appears before invoking the channel events, and the seond is called from the listener.

![Messages in consoe](./Images/example-result.png) 

As you can see, the `Player` and `AudioEngine` components absolutely don't need to know and reference each other to communicate!

## Advanced features

- [Create custom channel types](./Runtime/channel-asset-base.md)
- [Create inspector for custom channel types](./Editor/channel-asset-editor-base.md)

---

[<= Back to summary](./README.md)