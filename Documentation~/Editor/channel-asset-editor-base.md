# Broadcaster - Runtime - `ChannelAssetTXEditorBase`

Base custom editor for channel assets.

## Create inspector for custom channel types

This package provides several channel types for basic data (boolean, integer, ...), and their associated custom inspectors. But when you create custom channel asset types, you must also implement a custom inspector in order to get all features ready such as invoke channel assets directly from that inspector.

To do so, you can simply inherit from `ChannelAssetTXEditorBase`, where X is the number of parameters of the event, starting at 0.

As an example, let's create a custom channel asset type for events that take a `Transform` component as parameter:

```cs
using UnityEngine;
using MuffinDev.Broadcaster;

[Menu("Custom/Transform")]
public class TransformChannelAsset : ChannelAssetBase<Transform> { }
```

Note the use of [`MenuAttribute`](./menu-attribute.md) that allow you to customize the menu used to create the channel.

If you create a channel asset of that type and select it, you won't see the *Manual Trigger* section as implemented for included channel assets. Let's implement `ChannelAssetT0EditorBase` to fix this:

```cs
using UnityEngine;
using UnityEditor;
using MuffinDev.Broadcaster.EditorOnly;

[CustomEditor(typeof(TransformChannelAsset))]
public class TransformChannelAssetEditor : ChannelAssetT0EditorBase<Transform> { }
```

![Transform channel asset inspector](../Images/transform-channel-asset.png)

## Public API

```cs
public abstract class ChannelAssetT0EditorBase<T0> : TEditor<ChannelAssetBase<T0>> { }
public abstract class ChannelAssetT1EditorBase<T0, T1> : TEditor<ChannelAssetBase<T0, T1>> { }
public abstract class ChannelAssetT1EditorBase<T0, T1, T2> : TEditor<ChannelAssetBase<T0, T1, T2>> { }
public abstract class ChannelAssetT1EditorBase<T0, T1, T2, T3> : TEditor<ChannelAssetBase<T0, T1, T2, T3>> { }
```

---

[<= Back to summary](./README.md)