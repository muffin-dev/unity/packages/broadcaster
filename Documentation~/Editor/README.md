# Broadcaster - Documentation - Editor

## Summary

- [`ChannelAssetTXEditorBase`](./channel-asset-editor-base.md): Base custom editor for channel assets.

---

[<= Back to package's summary](../README.md)